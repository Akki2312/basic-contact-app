import React , {useState,useEffect} from "react";
import {BrowserRouter as Router,Switch,Route} from  'react-router-dom';
import {uuid} from 'uuidv4'
import './App.css';
import Header from "./Header"
import AddContact from "./AddContact"
import ContactList from "./ContactList"
import ContactDetail from "./ContactDetail";
 
//We are going to use react hook useState() for our functional component app() and its an empty array of conttacts
function App() {
  const LOCAL_STORAGE_KEY = "contacts";
  const [contacts, setContacts] = useState([]);

  const addContactHandler = (contact) => {
    setContacts([...contacts, {id: uuid(), ...contact }])
  };

  const removeContactHandler = (id) => {
    const newContactList = contacts.filter((contact) => {
    return contact.id !== id;
  });
   setContacts(newContactList);
  };

  useEffect(()=>{
    const getContactDeatils = JSON.parse(localStorage.getItem(LOCAL_STORAGE_KEY))
    if (getContactDeatils) setContacts(getContactDeatils);
    },[])
  
  useEffect(()=>{
  localStorage.setItem(LOCAL_STORAGE_KEY, JSON.stringify(contacts))
  },[contacts]) 
  // Contactlist has a property name contacts and we are passing prop in {} brackets
  return (
    <div className="ui container"> 
      <Router>
      <Header />
      <Switch>
        <Route 
          path= "/" 
          exact 
          render={(props) => (
            <ContactList 
            {...props}
            contacts={contacts} 
            getContactId={removeContactHandler} 
            />
          )}
        />
        <Route 
          path="/add" 
          render={(props) => (
            <AddContact {...props} addContactHandler={addContactHandler}/>
          )}
        />
          <Route path="/contact/:id" component={ContactDetail} />
      </Switch>
      </Router>
    </div>
  );
}

export default App;
